package com.kanin.component;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */


import com.werapan.databaseproject.model.Product;

/**
 *
 * @author punya
 */
public interface BuyProductable {
    public void buy(Product product,int qty);
}
